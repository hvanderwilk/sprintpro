module page/sign_out

page sign_out {
  init {
    logout();
    
    return root();
  }
}

access control rules
   rule page sign_out{ loggedIn() }

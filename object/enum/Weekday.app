module object/enum/Weekday

enum Weekday {
  weekdayMonday("Monday"),
  weekdayTuesday("Tuesday"),
  weekdayWednesday("Wednesday"),
  weekdayThursday("Thursday"),
  weekdayFriday("Friday")
}
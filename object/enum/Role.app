module object/enum/Role

enum Role {
  roleAdmin("Admin"),
  roleManager("Manager"),
  roleReporter("Reporter")
}
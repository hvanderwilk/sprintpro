module entity/TaskProgress

entity TaskProgress {
	task            : Task
	updatedByUser   : User
  status          : TaskProgressStatus (not null)
  note            : WikiText
  day             : Weekday
}
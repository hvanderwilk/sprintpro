module entity/Project

entity Project {
  name          : String (id, validate(name.length() > 0, "Project name must not be empty"),  validate(isUniqueNameForGroup(), "Project name " + name + " already exists."))
  group         : Group (not null, allowed=getAllOrganizationGroup(), validate(group != null, "Projects must have an associated group"))
  
  cached function getAllProjectMemberUser : List<User> {
    var allProjectMember := from ProjectMember as pm where pm.project=~this;
    var allUser := List<User>();
    
    for (projectMember : ProjectMember in allProjectMember) {
      allUser.add(projectMember.user);
    }
    
    return allUser;
  }
  
  cached function getAllOrganizationMemberUser : List<User> {
    return from User as u where u.organization = ~this.group.organization;
  }
  
  function isUniqueNameForGroup: Bool {
    return this.name != null && (from Project as p where p.group = ~this.group and p.name = ~this.name and p.id != ~this.id limit 1).length == 0;
  }
  
  function getGroup (): Group {
    return this.group;
  }
  
  cached function getAllOrganizationGroup : List<Group> {
    return from Group as g where g.organization = ~securityContext.principal.organization;
  }
}
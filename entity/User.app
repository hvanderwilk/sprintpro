module entity/User

entity User {
  nameFirst   : String (validate(nameFirst.length() > 0, "First name must not be empty."))
  nameLast    : String (validate(nameLast.length() > 0, "Lasts name must not be empty."))
  email       : Email (validate(email != null, "Users must have an email registered."))
  name        : String (id, validate(isUniqueUserForOrganization(), "There is already a user with this name in the organization."))
  password    : Secret (validate(password.length() >= 8, "Password needs to be at least 8 characters") 
              , validate(/[a-z]/.find(password), "Password must contain a lower-case character") 
              , validate(/[A-Z]/.find(password), "Password must contain an upper-case character") 
              , validate(/[0-9]/.find(password), "Password must contain a digit"))
  organization: Organization (validate(securityContext.principal == null || securityContext.principal.organization == organization, "Cannot add members to another oganization."))
  role        : Role (not null, allowed=List<Role>(roleReporter, roleManager, roleAdmin),validate(role != null, "Users must have a role."))
  
  function isUniqueUserForOrganization: Bool {
     return securityContext.principal == null || (from User as u where u.organization = ~this.organization and u.name = ~this.name and u.id != ~this.id limit 1).length == 0;
  }
  
  function isAdmin: Bool {
    return this.role == roleAdmin;
  }
  
  function serialize(): JSONObject {
      var responseBody := JSONObject();
      
      responseBody.put ("nameFirst",  nameFirst);
      responseBody.put ("nameLast",  nameLast);
      responseBody.put ("email",  email);
      responseBody.put ("username",  name);
      responseBody.put ("organization",  organization.name);
      responseBody.put ("role", role.name );
      
      return responseBody;
   }
}
# Build webdsl
FROM frekele/ant:1.10.3-jdk8
RUN apt update && apt install -y wget unzip
RUN wget https://buildfarm.metaborg.org/job/webdsl-compiler/lastSuccessfulBuild/artifact/webdsl.zip
RUN unzip webdsl.zip
ENV PATH "$PATH:/root/webdsl/bin"
# Sanity check
RUN webdsl version
# Actually build project
COPY . /webdsl-project
RUN cd /webdsl-project && webdsl clean war
# Now launch Tomcat
FROM tomcat:7-jdk8-corretto
COPY --from=0 /webdsl-project/*.war /usr/local/tomcat/webapps
EXPOSE 8080
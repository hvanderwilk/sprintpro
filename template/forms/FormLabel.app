module template/forms/FormLabel

template formLabel( label: String ){
  label( label )[ class= "form-label" ]{
    elements
  }
}